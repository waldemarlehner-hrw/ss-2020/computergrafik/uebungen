public class Color{
  public byte r,g,b,a;
}


final int sizeX = 400;
final int sizeY = 400;
final int maxDeviation = 10;

final int circleMinRadius = 10;
final int circleMaxRadius = 50;
Color baseColorToDeviate;
void setup(){
  size(400,400);
  baseColorToDeviate = RNGColor(false);
  background((int)baseColorToDeviate.r,(int)baseColorToDeviate.g,(int)baseColorToDeviate.b);
}

void draw(){
  int circlePosX = (int)random(0,sizeX);
  int circlePosY = (int)random(0,sizeY);
  float circleSize = random(circleMinRadius,circleMaxRadius);
  
  Color randomColor = RNGColor(true);
  
  fill((int)randomColor.r,(int)randomColor.g,(int)randomColor.b,(int)randomColor.a);
  noStroke();
  circle(circlePosX,circlePosY,circleSize);
}

Color RNGColor(boolean isOpaque){
 Color returnVal = new Color();
 returnVal.r = RandomByte();
 returnVal.g = RandomByte();
 returnVal.b = RandomByte();
 if(!isOpaque){
   returnVal.a = (byte)255;
 }
 else{
   returnVal.a = RandomByte();
 }
 return returnVal;
}

Color DeviateColor(Color c, int maxDeviation){
  Color retColor = new Color();
  retColor.r = (byte)DeviateValue((int)c.r,maxDeviation,0,255);
  retColor.g = (byte)DeviateValue((int)c.g,maxDeviation,0,255);
  retColor.b = (byte)DeviateValue((int)c.b,maxDeviation,0,255);
  retColor.a = (byte)DeviateValue((int)c.a,maxDeviation,0,255);
  return retColor;
}

int DeviateValue(int value, int maxDeviation, int clampMin, int clampMax){
  float deviation = random(0,maxDeviation);
  int newValue = value + (int)deviation;
  if(newValue < clampMin)
    return clampMin;
  if(newValue > clampMax)
    return clampMax;
  return newValue;
}

byte RandomByte(){ return (byte)random(255);}
