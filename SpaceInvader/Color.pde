public class Color{
  
  private int h,s,v;
  Color(int hue, int saturation, int value){
    //Clamp values
    h = hue % 360;
    if(h < 0)
      h += 360;
    s = saturation;
    if(s < 0) s = 0;
    else if(s > 100) s = 100;
    v = value;
    if(v < 0) v = 0;
    else if (v > 100) v = 100;
    print("HSV:",h,s,v,"\n");
  }
  public void SetAsFill(){
    fill(h,s,v);
    
  }
}
