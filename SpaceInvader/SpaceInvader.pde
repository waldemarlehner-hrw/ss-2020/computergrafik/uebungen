import processing.svg.*;




/* CONFIG
*/
int ColorCount = 10;
int ColorMinSaturation = 40;
int ColorMaxSaturation = 100;
int ColorMinValue = 60;
int ColorMaxValue = 100;

int FullBlackPercentage = 50;


Color[] RNGColors;

void setup(){
  size(510,500);
  //size(50,50);
  blendMode(REPLACE);
  
  noStroke();
  colorMode(HSB,360,100,100);
  background(50,100,100);
  
  //Generate random set of colors
  RNGColors = new Color[ColorCount];  
  for(int i = 0; i < ColorCount; i++){
    RNGColors[i] = new Color(
      (int)random(0,360),
      (int)random(ColorMinSaturation,ColorMaxSaturation),
      (int)random(ColorMinValue,ColorMaxValue)
    );
  }
  
}



void draw(){
  //Clear
  beginRecord(SVG,"output/invader_####.svg");
  fill(0,0,100);
  rect(0,0,width,height);
  
  generateNewInvader();
  
  endRecord();
  delay(1000);

}





void generateNewInvader(){

  //Iterate Height
  for(int y = 0;y < height/10; y++){
    for(int x = 0; x < (width / 20);x++){
      //Do the left side, and mirror to right side
      //Check if Tile should be Full Black
      if(RNGBoolean(FullBlackPercentage)){
        fill(360,0,0);
      }
      else{changeToAnyColor();}
        rect(x*10,y*10,10,10);
        //Mirror
        rect(((width/10)-1-x)*10,y*10,10,10);
      
    }
  }
  //If is Even, middle Row needs to be done aswell
  int middleX = floor((width/10)/2);
  for(int y = 0; y < height / 10; y++){
     if(RNGBoolean(FullBlackPercentage)){
        fill(360,0,0);
      }
      else{changeToAnyColor();}
        rect(middleX*10,y*10,10,10);
  }
  
  
}


boolean RNGBoolean(int propabilityPercentage){
  float RNGvalue = random(0,100);
  return RNGvalue < propabilityPercentage;
}

void changeToAnyColor(){
  Color selectedColor = RNGColors[round(random(0,ColorCount-1))];
  selectedColor.SetAsFill();
}
