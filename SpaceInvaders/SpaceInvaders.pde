/* CONFIGURATION */

final int CFG_MIN_HUE = 0;       // 0 <-> 360
final int CFG_MAX_HUE = 360;       // 0 <-> 360
final int CFG_MIN_SATURATION = 50; // 0 <-> 100
final int CFG_MAX_SATURATION = 0;// 0 <-> 100
final int CFG_MIN_VALUE = 100;      // 0 <-> 100
final int CFG_MAX_VALUE = 100;     // 0 <-> 100
final int CFG_MIN_EMPTY = 50;      // 0 <-> 100
final int CFG_MAX_EMPTY = 80;      // 0 <-> 100

final int CFG_MIN_SECONDARY_HUESHIFT = 10;   // any int
final int CFG_MAX_SECONDARY_HUESHIFT = 20;  // any int

final int CFG_MIN_TERTIARY_HUESHIFT = 100;  // any int
final int CFG_MAX_TERTIARY_HUESHIFT = -100;  // any int


final int CFG_RESOLUTION = 5; // > 0   [i] An Invader's Resolution
final int CFG_BORDER = 5; // >= 0      [i] Border size between 2 invaders
final int CFG_XCOUNT = 100; // > 0      [i] Column Count
final int CFG_YCOUNT = 100; // > 0      [i] Row Count

final int CFG_COLOR1_WEIGHT = 10; //    [i] Primary Color Weight
final int CFG_COLOR2_WEIGHT = 4; //    [i] Secondary Color Weight
final int CFG_COLOR3_WEIGHT = 2; //    [i] Tertiary Color Weight

/* END CONFIGURATION */







class IntTuple{
    public int a,b;
    public IntTuple(int _a,int _b){
        a = _a;
        b = _b;
    }
}

class ColorSettings{

   public IntTuple Hue, Saturation, Value, SecondaryHueShift, TertiaryHueShift, BlackPercent; 
   public int weightPrimary, weightSecondary, weightTertiary;

    public ColorSettings(
        int minHue,
        int maxHue,
        int minSaturation, 
        int maxSaturation, 
        int minValue, 
        int maxValue, 
        int minSecondaryHueShift, 
        int maxSecondaryHueShift, 
        int minTertiaryHueShift,
        int maxTertiaryHueShift,
        int minBlackPercent,
        int maxBlackPercent,
        int primaryColorWeight,
        int secondaryColorWeight,
        int tertiaryColorWeight
    )
    {

            Hue = new IntTuple(minHue,maxHue);
            Saturation = new IntTuple(minSaturation,maxSaturation);
            Value = new IntTuple(minValue,maxValue);
            SecondaryHueShift = new IntTuple(minSecondaryHueShift,maxSecondaryHueShift);
            TertiaryHueShift = new IntTuple(minTertiaryHueShift, maxTertiaryHueShift);
            BlackPercent = new IntTuple(minBlackPercent,maxBlackPercent);
            weightPrimary = primaryColorWeight;
            weightSecondary = secondaryColorWeight;
            weightTertiary = tertiaryColorWeight;
    }


    
}

class Color{
    public int h,s,v;

    public Color(int hue, int saturation, int value){
        //Clamp values
        h = hue % 360;
        if(h<0){
            h += 360;
        }
        s = saturation;
        if(s < 0){
            s = 0;
        }
        if(s > 100){
            s = 100;
        }
        v = value;
        if(v < 0){
            v = 0;
        }
        if(v > 100){
            v = 100;
        }
    }
    
}




class Drawer{

    IntTuple count;
    int gap;
    ColorSettings settings;
    int resolution;

    public Drawer(int xCount, int yCount, int invaderResolution, int gapPx, ColorSettings _settings){
        count = new IntTuple(xCount,yCount);
        gap = gapPx;
        settings = _settings;
        resolution = invaderResolution;
    }


    public void Draw(){
 
        //Calculate Required Canvas Size
                    //Border left and right + xCount-1 times Border + xCount times InvaderResolution
        int xPixels = 2*gap + (count.a-1) * gap + count.a * resolution;
        int yPixels = 2*gap + (count.b-1) * gap + count.b * resolution;

        PImage img = createImage(xPixels, yPixels, ARGB);
        
        for(int x = 0; x < count.a; x++){
            int hue = round(float(x)/float(count.a-1) * (settings.Hue.b - settings.Hue.a) + settings.Hue.a);
          
            print("Hue"+hue+"\n");
            for(int y = 0; y < count.b; y++){
                //Main Color
                
                 //<>//
                int value =  round((1-(float(y)/float(count.b-1))) * (settings.Value.b - settings.Value.a) + settings.Value.a);
                int saturation =  settings.Saturation.b - round(float(y)/float(count.b-1) * (settings.Saturation.b - settings.Saturation.a) + settings.Saturation.a);
                
                print("V/S : "+value+" / "+saturation+";\n");
                Color mainColor = new Color(hue,saturation,value);
                //Secondary
                Color secondaryColor = new Color(hue+round(random(settings.SecondaryHueShift.a,settings.SecondaryHueShift.b)),round(random(0,100)),round(random(0,100)));
                //Tertiary
                Color tertiaryColor = new Color(hue+round(random(settings.TertiaryHueShift.a,settings.TertiaryHueShift.b)),round(random(0,100)),round(random(0,100)));
                //
                int blackPercentage =round(float(y)/float(count.b-1) * (settings.BlackPercent.b - settings.BlackPercent.a) + settings.BlackPercent.a);
                
                DrawInvader(img,gap+x*(gap+resolution), gap+y*(gap+resolution),mainColor,secondaryColor,tertiaryColor, blackPercentage,settings.weightPrimary, settings.weightSecondary, settings.weightTertiary);
            }
        }
        img.save("output/out.png");
    }
    //xPxOffset and yPxOffset mark the top left corner of the invader
    void DrawInvader(PImage img,int xPxOffset, int yPxOffset, Color main, Color secondary, Color tertiary, int blackPercentage,int weight1, int weight2,int weight3){
      
        //Generate Weight Bounds
        IntTuple w1 = new IntTuple(0,weight1);
        IntTuple w2 = new IntTuple(weight1+1,weight1+1+weight2);
        int wmax = weight1+1+weight2+1+weight3;
        IntTuple w3 = new IntTuple(weight1+1+weight2+1, wmax);
        
        //Do left side of vertical mirror axis
        for(int x = 0; x < floor(resolution/2)+1; x ++){
          for(int y = 0; y < resolution; y++){
            //Check if is a Black Pixel, if so.. skip
            if(round(random(0,100)) < blackPercentage){
              continue;
            }
            //Now get the Color to be chosen
            int colorValue = floor(random(0,wmax));
            Color chosenColor;
            if(colorValue < w2.a){
            //Color is Primary
            chosenColor = main;
            }
            else if(colorValue > w1.b && colorValue < w3.a){
            //Color is Secondary
            chosenColor = secondary;
            }
            else if(colorValue > w2.b && colorValue < w3.b){
            //Color is Tertiary
            chosenColor = tertiary;
            }
            else{
             print("ERR: "+colorValue+"\n");
             chosenColor = new Color(0,0,0); //<>//
            }
            color c = color(chosenColor.h, chosenColor.s, chosenColor.v);
            //Now paint the Pixel with the color
            img.set(xPxOffset+x, yPxOffset+y,c);
            
            //Mirror the Pixel
            img.set(xPxOffset+resolution-x-1,yPxOffset+y,c);
            
            
          }
        }
        
        
        
        
    }
}


void setup(){
  print("Starting...\n");
  colorMode(HSB,360,100,100);
  
  generate();
}


void generate(){
  ColorSettings settings = new ColorSettings(
    CFG_MIN_HUE,
    CFG_MAX_HUE,
    CFG_MIN_SATURATION,
    CFG_MAX_SATURATION,
    CFG_MIN_VALUE,
    CFG_MAX_VALUE,
    CFG_MIN_SECONDARY_HUESHIFT,
    CFG_MAX_SECONDARY_HUESHIFT,
    CFG_MIN_TERTIARY_HUESHIFT,
    CFG_MAX_TERTIARY_HUESHIFT,
    CFG_MIN_EMPTY,
    CFG_MAX_EMPTY,
    CFG_COLOR1_WEIGHT,
    CFG_COLOR2_WEIGHT,
    CFG_COLOR3_WEIGHT
    
  );
  
  Drawer drawer = new Drawer(CFG_XCOUNT,CFG_YCOUNT,CFG_RESOLUTION,CFG_BORDER,settings);
  print("Drawer set up... \n");
  drawer.Draw();
  exit();
}
  
