// CONFIG

final int fontHeightPx = 8;
final int gap = 0;
final String messageURL = "https://raw.githubusercontent.com/Joe-Darling/Text_To_Image_Converter/master/bee%20movie%20script.txt";


// END CONFIG
PImage input;
PGraphics output;
PFont font;
int SrcWidth,SrcHeight;
int msgPtr = 0;
color[][] tileColors;
int cellLength = 2*gap+fontHeightPx;
String message;
void setup(){
  input = loadImage("input/in.png");
  input.loadPixels();
  //output = createImage(input.width - input.width % cellLength, input.height - input.height % cellLength, RGB);
  output = createGraphics(input.width - input.width % cellLength, input.height - input.height % cellLength);
  font = createFont("font.ttf",16);
 
  message = GetMessage();
  
  tileColors = GetTileColors();
  output.noSmooth();
  output.noStroke();
  output.beginDraw();
  output.background(0);
  output.textFont(font,fontHeightPx);
  PopulateTiles();
  output.endDraw();
  output.save("output/out.png");
  exit();
}


color[][] GetTileColors(){
  //Tile is set up by using the font height for height and width and using a gap on each side. → Theres always a square
  
  int xCount = input.width / cellLength;
  int yCount = input.height / cellLength;
  color retArray[][] = new color[xCount][yCount];
  for(int y = 0; y < yCount;y++){
    for(int x = 0; x < xCount; x++){
      retArray[x][y] = GetTileColor(x,y);
    }
  }
  return retArray;
}

color GetTileColor(int TileX, int TileY){
  int xTopLeft = TileX * cellLength;
  int yTopLeft = TileY * cellLength;
  //Get average color
  long rSum = 0;
  long gSum = 0;
  long bSum = 0;
  
  for(int y = 0; y < cellLength; y++){
    for(int x = 0; x < cellLength ; x++){
      color c = input.get(x+xTopLeft,y+yTopLeft);
      //RGB Color: 0xRRGGBB
      int r = c >> 16&0xFF;
      int g = c >> 8 & 0xFF;
      int b = c & 0xFF;
      rSum += r;
      gSum += g;
      bSum += b;
    }
  }
  int divider = cellLength * cellLength;
  print(rSum,gSum,bSum,"\n");
  return color(rSum / divider, gSum / divider, bSum / divider);
}

String GetMessage(){
  StringBuilder sb = new StringBuilder();
  final char[] charsToBeOmmitted = {'\b','!','.','\"','\'','-',','};
  String[] lines = loadStrings(messageURL);
  for(String line: lines){
    if(line.length() == 0)
      continue;
    String s = line.toUpperCase();
    sb.append(s);
    sb.append(" ");
  }
  String out =  sb.toString();
  for(char replace : charsToBeOmmitted){
    out = out.replace(str(replace),"");
  }

  return out;
}


void PopulateTiles(){
  int xCount = input.width / cellLength;
  int yCount = input.height / cellLength;
  for(int y = 0; y < yCount;y++){
    for(int x = 0; x < xCount; x++){
      //Get coords to bottom left anker for text
      int ankerX = x * cellLength + gap;
      int ankerY = (y+1) * cellLength + gap;
      char c = message.charAt(msgPtr++);
    
      if(msgPtr >= message.length())
        msgPtr = 0;
      String _c = str(c);
      output.fill(tileColors[x][y]);
      output.text(_c,ankerX,ankerY);
      
    }
  }
}
