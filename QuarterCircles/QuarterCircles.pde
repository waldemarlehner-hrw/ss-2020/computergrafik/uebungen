import processing.svg.*;

/* CONFIGURATION */
final int CFG_xRows = 50;
final int CFG_yRows = 50;
final int CFG_resolution = 30;    // in px, what's an items height and width
final int CFG_gap = 2;            // in px, what's the gap between 2 items
final int CFG_hue = 320;          // The default hue value to be used [0;360]
final int CFG_maxHueDeviation = 10; //The max deviation from the default hue [0;180]
final int CFG_emptyCellPercentage = 10; // How many cells chould be kept empty? [0;100]
final int CFG_minSaturation = 50;     // [0;100]
final int CFG_maxSaturation = 80;    // [0;100]
final int CFG_minValue = 60;          // [0;100]
final int CFG_maxValue = 100;         // [0;100]
/* END CONFIGURATION */

PGraphics pg;

void setup(){
    //hint(DISABLE_ASYNC_SAVEFRAME);
    colorMode(HSB,360,100,100);
    int graphicsWidth =  CFG_gap + CFG_xRows * (CFG_resolution + CFG_gap);
    int graphicsHeight = CFG_gap + CFG_yRows * (CFG_resolution + CFG_gap);

   
    pg = createGraphics(graphicsWidth, graphicsHeight,SVG,"output/out.svg");
    
    pg.noStroke();
    pg.noSmooth();
    pg.beginDraw(); //<>//
    color fullwhite = color(121,100,100);
    pg.fill(fullwhite); // Full white
    
    createImage();
    pg.endDraw();
    //pg.save("output/out2.pdf");
    
    exit();
}

void createImage(){
    for(int x = 0 ; x < CFG_xRows; x++){
        for(int y = 0; y < CFG_yRows; y++){
            if(round(random(0,100)) < CFG_emptyCellPercentage)
                continue;
            int hue = CFG_hue + (round(random(-CFG_maxHueDeviation,CFG_maxHueDeviation)));
            hue = hue % 360;
            if( hue < 0){
                hue += 360;
            }
            int saturation = round(random(CFG_minSaturation, CFG_maxSaturation));
            int value = round(random(CFG_minValue,CFG_maxValue));
            color angleColor = color(hue,saturation,value);
            print(hue," ",saturation," ",value,"\n");
            createArc(CFG_gap+ x * (CFG_resolution + CFG_gap),CFG_gap+ y * (CFG_resolution + CFG_gap),angleColor);

        }
    }
}

void createArc(int topLeftOffsetX, int topLeftOffsetY,color c){
    //Get a random value between 0 and 100. 
    //According to this value to arcs orientation will be determined
    int RNGNumber = round( random(0,100) );

    int orientation; 
    if(RNGNumber <= 25){        // Top Left
        orientation = 0;
    }else if(RNGNumber <= 50){  // Top Right
        orientation = 1;
    }else if(RNGNumber <= 75){  // Bottom Right
        orientation = 2;
    }else {                     // Bottom Left
        orientation = 3;
    }

    pg.fill(c);
    //using RADIUS mode. Anker == Center
    int xAnker = topLeftOffsetX;
    int yAnker = topLeftOffsetY;
    
    if(orientation == 2 ||orientation == 3)
        yAnker -= CFG_resolution;
    if(orientation == 1 || orientation == 2)
        xAnker -= CFG_resolution;
    
    float angle = HALF_PI*orientation+PI;
    
    pg.arc(xAnker,yAnker,CFG_resolution*2,CFG_resolution*2,angle, angle + HALF_PI );


}
